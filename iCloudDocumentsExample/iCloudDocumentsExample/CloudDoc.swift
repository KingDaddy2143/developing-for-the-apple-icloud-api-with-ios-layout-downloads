//
//  CloudDoc.swift
//  iCloudDocumentsExample
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class CloudDoc: UIDocument {
    
    // load method (the data has been received from the cloud)
    override func loadFromContents(contents: AnyObject, ofType typeName: String?) throws {
        guard let loadedData: String = String(data: contents as! NSData, encoding: NSUTF8StringEncoding) else{
            print("Error loading data")
            return
        }
        mainViewController.textView.text = loadedData
    }
    
    //This method is used for save the data to the cloud
    override func contentsForType(typeName: String) throws -> AnyObject {
        return mainViewController.textView.text.dataUsingEncoding(NSUTF8StringEncoding)!
    }
}
