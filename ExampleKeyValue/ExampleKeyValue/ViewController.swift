//
//  ViewController.swift
//  ExampleKeyValue
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loadTextField: UITextField!
    @IBOutlet weak var saveTextField: UITextField!
    let key: String = "key"
    var store : NSUbiquitousKeyValueStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        store =  NSUbiquitousKeyValueStore.defaultStore()
        
        //setting nsnotification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.didReceiveNotification(_:)), name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification, object: store)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveDataAction(sender: AnyObject) {
        if saveTextField.text == "" || saveTextField.text == nil{
            print("return save")
            return
            
        }
        store.setString(saveTextField.text, forKey: key)
        store.synchronize()
    }
    @IBAction func loadAction(sender: AnyObject) {
        if store.stringForKey(key) == nil{
            print("return")
            return
        }
        self.loadData()
     }
    
    func loadData()  {
        loadTextField.text = store.stringForKey(key)

    }
    
    //receive notification and update view
    func didReceiveNotification(notification: NSNotification){
        loadData()
    }

}

